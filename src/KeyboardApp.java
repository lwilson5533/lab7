import java.util.*;
/**--------------------------------------------------------
 * KeyboardApp.java:
  * Pig Latin Translator
  * 
  * 
  * @author txr185
  */

public class KeyboardApp 
{
    //--------------------------------------------------------
    /** Creates a KeyBoardApp.
      *
      * @param args String[]
      */
    public static void main( String args[] )
    {
        String question = "Enter a word and I will translate it to Pig Latin!";
        System.out.println(question);
        Scanner scanner = new Scanner(System.in);
        String input = new String();

        while (!input.equalsIgnoreCase("quit"))
        {
            if (scanner.hasNext())
            {
                input = scanner.next();
                if (!input.equalsIgnoreCase("quit"))
                {
                    System.out.println(StringUtilities.translate(input));
                }
            }
        }

    }
}
